<header id="top" class="header">
      <div class="vertical-center">
        <img class="header__logo" src="http://geeklabel.netlify.com/images/logo.png" alt="geek label logo">
          <h1 class="sr-only">Geek Label</h1>
      <p class="header__text">A team of self-confessed geeks who are all about great digital design</p>
        <a href="#firstIntro" class="next-section-link next-section-link--solid">
          <img class = "next-section-link__image" src="http://geeklabel.netlify.com/images/page-down-style-3.png" alt="Next section">
        </a>
      </div>
  </header>

  <section id="firstIntro" class="introduction">
    <div class="vertical-center">
      <img src="http://geeklabel.netlify.com/images/ill-1.png" alt="Illustration one" class="introduction__illustraion introduction__illustraion--one">
      <div class="container">
        <h3 class="introduction__text">
          Some agencies love <span class="introduction__text--special">design</span>, but don't know how to build
        </h3>
        <a href="#midIntro" class="next-section-link next-section-link--transparent">
          <img class = "next-section-link__image" src="http://geeklabel.netlify.com/images/page-down-style-3.png" alt="Next section">
        </a>
      </div>
    </div>
  </section>

  <section id="midIntro" class="introduction">
    <div class="vertical-center">
      <img src="http://geeklabel.netlify.com/images/ill-2.png" alt="Illustration one" class="introduction__illustraion introduction__illustraion--one">
      <div class="container">
        <h3 class="introduction__text">
          Some agencies know how to <span class="introduction__text--special">build</span>, but can't do design
        </h3>
        <a href="#finalIntro" class="next-section-link next-section-link--transparent">
          <img class = "next-section-link__image" src="http://geeklabel.netlify.com/images/page-down-style-3.png" alt="Next section">
        </a>
      </div>
    </div>
  </section>

  <section id="finalIntro" class="introduction">
    <div class="vertical-center">
      <img src="http://geeklabel.netlify.com/images/ill-3.png" alt="Illustration one" class="introduction__illustraion introduction__illustraion--one">
      <div class="container">
        <h3 class="introduction__text">
          We love <span class="introduction__text--special">both</span>
        </h3>
        <a href="#services" class="next-section-link next-section-link--transparent">
          <img class = "next-section-link__image" src="http://geeklabel.netlify.com/images/page-down-style-3.png" alt="Next section">
        </a>
      </div>
    </div>
  </section>

  <section id="services" class="services">
    <div class="container">
      <h2>Services</h2>
      <div class="row services__list">
        <div class="col-md-3 service clearfix">
          <div class="img-responsive service__image">
            <img src="http://geeklabel.netlify.com/images/service-web-dev.png" alt="Web Develpment">
          </div>
          <p>Web Development</p>
        </div>
        <div class="col-md-3 service clearfix">
          <div class="img-responsive service__image">
            <img src="http://geeklabel.netlify.com/images/service-brush.png" alt="Design">
          </div>
          <p>Design</p>
        </div>
        <div class="col-md-3 service clearfix">
          <div class="img-responsive service__image">
            <img src="http://geeklabel.netlify.com/images/service-branding.png" alt="Branding">
          </div>
          <p>Branding</p>
        </div>
        <div class="col-md-3 service clearfix">
          <div class="img-responsive service__image">
            <img src="http://geeklabel.netlify.com/images/service-ux-research.png" alt="UX Research">
          </div>
          <p>UX Research</p>
        </div>
      </div>
      <a href="#clients" class="next-section-link next-section-link--transparent">
        <img class = "next-section-link__image" src="http://geeklabel.netlify.com/images/page-down-style-1.png" alt="Next section">
      </a>
    </div>
  </section>

  <section id="clients" class="clients container clients">
    <h2>Clients</h2>
    <button class="arrow prev-arrow">
      <span class="sr-only">Prev</span>
      <img src="http://geeklabel.netlify.com/images/prev-caret.png" alt="Previous Client">
    </button>
    <button class="arrow next-arrow">
      <span class="sr-only">Next</span>
      <img src="http://geeklabel.netlify.com/images/next-caret.png" alt="Next Client">
    </button>
    <div class="client__carousel">
      <div class="client__logo">
        <img src="http://geeklabel.netlify.com/images/cl-photographers.png" alt="Photographers">
      </div>
      <div class="client__logo">
        <img src="http://geeklabel.netlify.com/images/cl-pshe.png" alt="PSHE">
      </div>
      <div class="client__logo">
        <img src="http://geeklabel.netlify.com/images/cl-vegan.png" alt="The Vegan Society">
      </div>
      <div class="client__logo">
        <img src="http://geeklabel.netlify.com/images/cl-photographers.png" alt="Photographers">
      </div>
      <div class="client__logo">
        <img src="http://geeklabel.netlify.com/images/cl-pshe.png" alt="PSHE">
      </div>
      <div class="client__logo">
        <img src="http://geeklabel.netlify.com/images/cl-vegan.png" alt="The Vegan Society">
      </div>
    </div>
    <a href="#address" class="next-section-link next-section-link--transparent">
      <img class = "next-section-link__image" src="http://geeklabel.netlify.com/images/page-down-style-1.png" alt="Next section">
    </a>
  </section>

  <section id="address" class="address">
    <h2>
      <span class="hidden-xs">How to find us</span>
      <span class="visible-xs-block">Find us</span>
    </h2>
    <div class="map-container">
      <div class="map" id="map"></div>
    </div>
    <a href="#contactform" class="next-section-link next-section-link--transparent">
      <img class = "next-section-link__image" src="http://geeklabel.netlify.com/images/page-down-style-3.png" alt="Next section">
    </a>
  </section>
  <section id="contactform" class="contact">
    <div class="container">
      <div class="row">
        <h2>Contact</h2>
        <div class="col-lg-12">
          <form class="contact-form center-block">
            <div class="form-group">
              <label class="sr-only" for="name">Name</label>
              <input type="text" class="form-control" id="name" placeholder="Name">
            </div>
            <div class="form-group">
              <label class="sr-only" for="email">Email address</label>
              <input type="email" class="form-control" id="email" placeholder="Email">
            </div>
            <div class="form-group">
              <label class="sr-only" for="message">Message</label>
              <!-- <input type="textarea" class="form-control" id="message" placeholder="Message"> -->
              <textarea class="form-control" name="message" id="message" rows="5" placeholder="Message"></textarea>
            </div>
            <button type="submit" class="btn btn-default--geeklab btn-block">Send Message!</button>
            <p class="text-center hidden-xs">Or phone on: 01923 220121</p>
            <div class="contact-form__info visible-xs-block">
              <p>
                <span class="glyphicon glyphicon-earphone"></span>
                01923 220121
              </p>
              <p>
                <span class="glyphicon glyphicon-envelope"></span>
                info@compucorp.co.uk
              </p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>