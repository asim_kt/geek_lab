'use strict';

/**
 *	Config
 */

module.exports = {

	// Path to build and app folders

	dist: './dist/',
	src: './app/',

	// Path to temporary folder for css

	tmp: '.tmp',

	// Flag to add hash for assets files

	rev: true,

	// Style type [css, less, sass, stylus]. Selected when generating project

	csstype: 'less',

	// Path to project folders [folder name or false or '']

	folder: {
		fonts: 'fonts',
		icons: '',
		images: 'images',
		pictures: '',
		pug: false,
		styles: 'less',
		scripts: 'scripts',
		vendors: 'bower_components'
	},

	// Browser prefixs for autoprefixer

	browsers: [
		'Firefox < 20',
		'Chrome < 20',
		'Opera < 12'
	],

	// Files list just for copy

	copyfiles: [
		'favicon.ico'
	]

};