# Geek Label - A LESS powered Drupal 7 theme
## About the project

Geek label is a new design agency that requires a new website. They are a team of self-confessed geeks who are all about great digital design.

This is a bonus task to extend a Bootstrap based theme. And it has more sections with a stylish design. I started with generating the static files with a yeoman generated directory.

### Approach

I've added bootstrap as a bower dependency since it was easy to maintain and keep the cleanliness of the repo. I've tried to use Bootstrap classes as much as possible and tried to implement the style in a Mobile first way to align with Bootstrap style guide.

### BEM and LESS

As in the open charity task, I've used BEM for this project too. I've divided the LESS files in a component way for easy maintenance. I've used LESS for Geek Label where I used SCSS for Open Charity task.

### JavaScript

The design consisted a client list carousel. To implement the same I've used Slick Carousel plugin. It was light and easy and it supports responsiveness out of the box. I couldn't find the correct font family used for the design, so I tried use a family which mostly matching with the design.

### To drupal 7

I've uploaded the site at http://dev-geek-label.pantheonsite.io/ since it came with a Git integration and have free plans to a limit. I've uploaded the static site at http://geeklabel.netlify.com too. I've hosted the theme files at **geeklabel-drupal7-theme-files/** of this repo.